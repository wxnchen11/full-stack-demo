#!/usr/bin/python3

from flask import Flask, render_template, request, jsonify
import grequests
from collections import defaultdict
import sqlite3
from pdb import set_trace as bp

app = Flask(__name__)

app.debug = True

POST_URL = "https://experimentation.getsnaptravel.com/interview/hotels"
PROVIDERS = ["snaptravel", "retail"]
DB_PATH = "hotels.db"

HOTELS_TABLE = """ CREATE TABLE IF NOT EXISTS hotels (
                                        hotel_id integer PRIMARY KEY,
                                        name text,
                                        num_reviews integer,
                                        address text,
                                        num_stars integer,
                                        image_url text,
                                        snap_price double,
                                        retail_price double
                                    ); """

AMENITIES_TABLE = """ CREATE TABLE IF NOT EXISTS amenities (
                                        name text UNIQUE
                                    ); """

REQUESTS_TABLE = """ CREATE TABLE IF NOT EXISTS requests (
                                        city text,
                                        checkin text,
                                        checkout text
                                    ); """

HOTELS_TO_AMENITIES_TABLE = """ CREATE TABLE IF NOT EXISTS hotels_to_amenities (
                                        hotel_id integer NOT NULL,
                                        amenities_id integer NOT NULL
                                    ); """

REQUESTS_TO_HOTELS_TABLE = """ CREATE TABLE IF NOT EXISTS requests_to_hotels (
                                        request_id integer NOT NULL,
                                        hotel_id integer NOT NULL
                                    ); """

TABLES = [
    HOTELS_TABLE,
    AMENITIES_TABLE,
    REQUESTS_TABLE,
    HOTELS_TO_AMENITIES_TABLE,
    REQUESTS_TO_HOTELS_TABLE
]

def initDB():
    con = create_connection(DB_PATH)
    cur = con.cursor()
    for table in TABLES:
        cur.execute(table)

def create_connection(db_file):
    try:
        conn = sqlite3.connect(db_file, isolation_level=None)
        return conn
    except Exception as e:
        print(e)
 
    return None

def insertDB(cur, table, values):
    s = "insert into %s values (%s)" % (table, ", ".join(['?']*len(values)))
    try:
        cur.execute(s, values)
    except Exception as e:
        print(e)
        bp()

def getHotel(cur, hid):
    s = "select * from hotels where hotel_id = ?"
    try:
        ret = cur.execute(s, (hid,)).fetchall()
        return ret
    except Exception as e:
        print(e)
        bp()

    return None

def getHotel(cur, hid):
    s = "select * from hotels where hotel_id = ?"
    try:
        ret = cur.execute(s, (hid,)).fetchall()
        return ret
    except Exception as e:
        print(e)
        bp()

    return None

def getRequestId(cur, city, checkin, checkout):
    s = "select rowid from requests where city=? and checkin=? and checkout=?"
    try:
        ret = cur.execute(s, (city, checkin, checkout)).fetchall()
        if len(ret) != 0:
            return ret[0][0]
        else:
            s = "insert into requests values (?,?,?)"
            print((city, checkin, checkout))
            ret = cur.execute(s, (city, checkin, checkout))
            return getRequestId(cur, city, checkin, checkout)
    except Exception as e:
        print(e)
        bp()

    return None

def getAmenityId(cur, amenity):
    s = "select rowid from amenities where name=?"
    try:
        ret = cur.execute(s, (amenity,)).fetchall()
        if len(ret) != 0:
            return ret[0][0]
        else:
            s = "insert into amenities values (?)"
            ret = cur.execute(s, (amenity,))
            return getAmenityId(cur, amenity)

    except Exception as e:
        print(e)
        bp()

    return None

def getHotelsFromRid(cur, rid):
    query = """select hotels.hotel_id,
            hotels.name,
            hotels.num_reviews,
            hotels.address,
            hotels.num_stars,
            hotels.image_url,
            hotels.snap_price,
            hotels.retail_price
            from requests_to_hotels
            inner join hotels
            on hotels.hotel_id = requests_to_hotels.hotel_id
            where request_id=?
            """
    try:
        res = cur.execute(query, (rid,)).fetchall()
        return res

    except Exception as e:
        print(e)
        bp()

    return None

def getAmenitiesForHotel(cur, hid):
    query = """select amenities.name
            from hotels_to_amenities
            inner join amenities
            on amenities.rowid = hotels_to_amenities.amenities_id
            where hotel_id=?
            """
    try:
        res = cur.execute(query, (hid,)).fetchall()
        return res

    except Exception as e:
        print(e)
        bp()

    return None

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/get_hotels')
def get_hotels():

    con = create_connection(DB_PATH)
    cur = con.cursor()

    city = request.args.get('city')
    checkin = request.args.get('checkin')
    checkout = request.args.get('checkout')

    reqid = getRequestId(cur, city, checkin, checkout)
    hotels = getHotelsFromRid(cur, reqid)

    ret = []

    if len(hotels) > 0:
        for h in hotels:
            ret.append({
                "id": h[0],
                "hotel_name": h[1],
                "num_reviews": h[2],
                "address": h[3],
                "num_stars": h[4],
                "image_url": h[5],
                "snaptravel_price": h[6],
                "retail_price": h[7],
                "amenities": getAmenitiesForHotel(cur, h[0])
            })

        return jsonify({ "data": ret })

    reqs = [] 

    for provider in PROVIDERS:
        body = {
            "city": city,
            "checkin": checkin,
            "checkout": checkout,
            "provider": provider
        }
        reqs.append(grequests.post(POST_URL, json=body))

    resps = grequests.map(reqs)

    appearance_count = defaultdict(int)

    hotels_to_add = {}

    for i, resp in enumerate(resps):
        l = resp.json()
        for hotel in l["hotels"]:
            hid = hotel["id"]
            appearance_count[hid] += 1
            if hid not in hotels_to_add:
                hotels_to_add[hid] = hotel
            hotels_to_add[hid][PROVIDERS[i] + "_price"] = hotel["price"]

    for hid, no in appearance_count.items():
        if no == len(PROVIDERS):
            ret.append(hotels_to_add[hid])
            if len(getHotel(cur, hid)) == 0:
                hotel = hotels_to_add[hid]
                v = [
                    hotel["id"],
                    hotel["hotel_name"],
                    hotel["num_reviews"],
                    hotel["address"],
                    hotel["num_stars"],
                    hotel["image_url"],
                ]
                for p in PROVIDERS:
                    v.append(hotel[p + "_price"])
                insertDB(cur, "hotels", tuple(v))

                for a in hotel["amenities"]:
                    aid = getAmenityId(cur, a)
                    insertDB(cur, "hotels_to_amenities", (hid, aid))

            insertDB(cur, "requests_to_hotels", (reqid, hid))

    return jsonify({ "data": ret })

if __name__ == "__main__":
    cur = initDB()
    app.run(port=8000)

    